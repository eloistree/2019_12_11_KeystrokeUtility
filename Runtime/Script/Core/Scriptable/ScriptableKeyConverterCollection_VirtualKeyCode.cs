﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WindowsInput.Native;

[CreateAssetMenu(fileName = "Virtual key converter", menuName = "ScriptableObjects/Virtual key converter", order = 1)]
public class ScriptableKeyConverterCollection_VirtualKeyCode : ScriptableObject
{
    public KeyConverterCollection_VirtualKeyCode m_filter = new KeyConverterCollection_VirtualKeyCode();

    public string Get(VirtualKeyCode keyState)
    {
        return m_filter.Get(keyState);
    }
}
