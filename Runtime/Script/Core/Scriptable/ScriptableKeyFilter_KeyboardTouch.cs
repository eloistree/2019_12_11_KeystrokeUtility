﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Keyboard Touch Filter", menuName = "ScriptableObjects/Keyboard Touch Filter", order = 1)]
public class ScriptableKeyFilter_KeyboardTouch : ScriptableObject
{
    public KeyFilterCollection_KeyboardTouch m_filter = new KeyFilterCollection_KeyboardTouch();
    public KeyboardTouch[] GetSelected()
    {
        return m_filter.GetSelected();
    }
}