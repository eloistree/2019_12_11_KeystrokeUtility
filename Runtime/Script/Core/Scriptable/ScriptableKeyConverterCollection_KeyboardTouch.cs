﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WindowsInput.Native;

[CreateAssetMenu(fileName = "Touch key Converter", menuName = "ScriptableObjects/Touch key Converter", order = 1)]
public class ScriptableKeyConverterCollection_KeyboardTouch : ScriptableObject
{
    public KeyConverterCollection_KeyboardTouch m_filter = new KeyConverterCollection_KeyboardTouch();

    public string Get(KeyboardTouch keyState)
    {
        return m_filter.Get(keyState);
    }
}
