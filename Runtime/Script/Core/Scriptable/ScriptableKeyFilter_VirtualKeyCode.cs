﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WindowsInput.Native;

[CreateAssetMenu(fileName = "Virtual key Filter", menuName = "ScriptableObjects/Virtual key Filter", order = 1)]
public class ScriptableKeyFilter_VirtualKeyCode : ScriptableObject
{
    public KeyFilterCollection_VirtualKeyCode m_filter = new KeyFilterCollection_VirtualKeyCode();

    public VirtualKeyCode[] GetSelected()
    {
        return m_filter.GetSelected();
    }
}
