﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using WindowsInput.Native;

public class UnityWindowSimListenerMono : MonoBehaviour
{
    public WindowSimListenerMono m_target;

    public Queue<TouchKeyChange> m_changeTouchFound = new Queue<TouchKeyChange>();
    public Queue<VirtualKeyChange> m_changeKeyFound = new Queue<VirtualKeyChange>();


    public VirtualKeyUnityEvent m_onChangeKey;
    public TouchKeyUnityEvent m_onChangeTouch;

    [System.Serializable]
    public class VirtualKeyUnityEvent : UnityEvent<VirtualKeyChange> { }
    [System.Serializable]
    public class TouchKeyUnityEvent : UnityEvent<TouchKeyChange> { }

    void Awake()
    {
        m_target.AddWindowChangeListener(ListenToTouch);
        m_target.AddWindowChangeListener(ListenToKey);
    }
    void OnDestroy()
    {
        m_target.RemoveWindowChangeListener(ListenToTouch);
        m_target.RemoveWindowChangeListener(ListenToKey);
    }

    private void ListenToTouch(bool pressState,  KeyboardTouch keyState)
    {
        m_changeTouchFound.Enqueue(new TouchKeyChange(keyState, pressState));
    }
    private void ListenToKey(bool pressState, VirtualKeyCode keyState)
    {
        m_changeKeyFound.Enqueue( new VirtualKeyChange(keyState, pressState));
    }

    public void DequeueInWaiting() {

        while (m_changeTouchFound.Count > 0)
        {
            m_onChangeTouch.Invoke(m_changeTouchFound.Dequeue());
        }
        while (m_changeKeyFound.Count > 0)
        {
            m_onChangeKey.Invoke(m_changeKeyFound.Dequeue());
        }
    }

    void Update()
    {
        DequeueInWaiting();


    }
}
