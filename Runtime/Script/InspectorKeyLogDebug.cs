﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InspectorKeyLogDebug : AbstractKeyLogDebug
{

    [TextArea(0,2)]
    public string m_currentlyDebug;

    [TextArea(0, 10)] 
    public string m_historyDebug;

    public override void SetCurrentText(string text)
    {
        m_currentlyDebug = text;
    }

    public override void SetHistoryText(string text)
    {
        m_historyDebug = text;
    }
}
