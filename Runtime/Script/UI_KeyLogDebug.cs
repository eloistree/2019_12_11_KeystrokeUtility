﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WindowsInput.Native;

public class UI_KeyLogDebug : AbstractKeyLogDebug
{

    public InputField m_currentlyUI;
    public InputField m_historyUI;

    public override void SetCurrentText(string text)
    {
        if (m_currentlyUI != null)
            m_currentlyUI.text =text;
    }

    public override void SetHistoryText(string text)
    {
        if (m_historyUI != null)
            m_historyUI.text = text;
    }
}

public abstract class AbstractKeyLogDebug : MonoBehaviour
{
    public List<string> m_currentlyActive = new List<string>();
    public List<string> m_history = new List<string>();

    public int m_historyCount=10;
    public bool m_changed;
    public void SetTrue(string keyDescription) { Set(true, keyDescription); }
    public void SetFalse(string keyDescription) { Set(false, keyDescription); }
    public void SetTrue(KeyboardTouch keyDescription) { Set(true, keyDescription.ToString()); }
    public void SetFalse(KeyboardTouch keyDescription) { Set(false, keyDescription.ToString()); }
    public void SetTrue(VirtualKeyCode keyDescription) { Set(true, keyDescription.ToString()); }
    public void SetFalse(VirtualKeyCode keyDescription) { Set(false, keyDescription.ToString()); }

    public void Set(TouchKeyChange change)
    {
        Set(change.GetPressState(), change.GetKey().ToString());
    }
    public void Set(VirtualKeyChange change)
    {
        Set(change.GetPressState(), change.GetKey().ToString());
    }
    public void Set(bool state, string keyDescription)
    {
        if (state)
            m_currentlyActive.Add(keyDescription);
        else
            m_currentlyActive.Remove(keyDescription);

        m_history.Insert(0, GetTextOf(state, keyDescription));
        if (m_history.Count > m_historyCount)
            m_history.RemoveAt(m_historyCount - 1);
        m_changed = true;
    }

    private string GetTextOf(bool state, string keyDescription, string on = "_", string off = "‾ ")
    {
        return (state ? on : off) + keyDescription;
    }
    void Update()
    {
        if (m_changed)
        {
            m_changed = false;
            RefreshUI();
        }

    }

    private void RefreshUI()
    {
        SetCurrentText(string.Join(" ", m_currentlyActive));
        SetHistoryText(string.Join("\n", m_history));
    }

    public abstract void SetCurrentText(string text);
    public abstract void SetHistoryText(string text);
}
