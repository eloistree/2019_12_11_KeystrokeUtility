﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using WindowsInput.Native;

public class EmittedStringFromWindowSimListener : MonoBehaviour
{
    public WindowSimListenerMono m_target;

    public bool m_useVirtualKeyboard=true;
    public ScriptableKeyConverterCollection_VirtualKeyCode m_windowKeyboard;
    public bool m_useAbstractKeyboard;
    public ScriptableKeyConverterCollection_KeyboardTouch m_abstractKeyboard;


    public ChangeKeyToTextFound m_emittedConversionCall;
    public StringEvent m_emittedVirtualOnDirectThread;
    public StringEvent m_emittedVirtualOnUnityThread;
    public Queue<string> m_emittedQueue = new Queue<string>();

    void Awake()
    {
        m_target.AddWindowChangeListener(TryToConvertTouch);
        m_target.AddWindowChangeListener(TryToConvertKey);
    }

    private void TryToConvertTouch(bool pressState, KeyboardTouch keyState)
    {
        if (pressState) { 
            if(m_useAbstractKeyboard)
                TryToConvert(m_abstractKeyboard.Get(keyState));
        }
    }
    private void TryToConvertKey(bool pressState, VirtualKeyCode keyState)
    {
        if (pressState)
        {

            if (m_useVirtualKeyboard)
                TryToConvert(m_windowKeyboard.Get(keyState));
        }
    }

    private void TryToConvert(string text)
    {
        if (string.IsNullOrEmpty(text))
            return;
        if (text.Length < 1)
            return;

        if(m_emittedConversionCall!=null)
            m_emittedConversionCall(text);
        m_emittedVirtualOnDirectThread.Invoke(text);
        m_emittedQueue.Enqueue(text);

    }
    private void Update()
    {
        while (m_emittedQueue.Count > 0) {
            m_emittedVirtualOnUnityThread.Invoke(m_emittedQueue.Dequeue());
        }

    }



    void OnDestroy()
    {
        m_target.RemoveWindowChangeListener(TryToConvertTouch);
        m_target.RemoveWindowChangeListener(TryToConvertKey);

    }
    public delegate void ChangeKeyToTextFound(string emitted);

    [System.Serializable]
    public class StringEvent : UnityEvent<string> { }
    public void AddChangeListener(ChangeKeyToTextFound listener)
    {
        m_emittedConversionCall += listener;
    }
    public void RemoveChangeListener(ChangeKeyToTextFound listener)
    {
        m_emittedConversionCall -= listener;
    }

}
