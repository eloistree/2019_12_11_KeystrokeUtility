﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WindowsInput;
using WindowsInput.Native;

public class WindowSimListenerMono : MonoBehaviour
{
    public WindowSimListener m_keyboardState = new WindowSimListener();


    [Header("Debug")]
    public VirtualKeyCode[] m_press;
    public VirtualKeyCode[] m_release;
    public KeyboardTouch[] m_pressTouch;

    [Header("Filter")]
    public ScriptableKeyFilter_VirtualKeyCode m_virtualKeyToListen;
    public ScriptableKeyFilter_KeyboardTouch m_keyboardTouchToListen;

    public bool useUpdateRefresh;

    public void Awake()
    {
        m_keyboardState.SetVirtualKey(m_virtualKeyToListen.GetSelected());
    }

    void Update()
    {
        if (useUpdateRefresh)
            m_keyboardState.RefreshState();
        m_press = m_keyboardState.GetPressing().ToArray();
        m_release = m_keyboardState.GetReleased().ToArray();
        m_pressTouch = GetKeytouchOf(m_press);

    }

    private KeyboardTouch[] GetKeytouchOf(VirtualKeyCode[] keys)
    {
        if (keys == null)
            return new KeyboardTouch[0];
        List<KeyboardTouch> found = new List<KeyboardTouch>();
            KeyboardTouch key;
            bool isConvertable;
        for (int i = 0; i < keys.Length; i++)
        {
            
            //KeyBindingTable.ConvertWindowVirtualKeyCodeToTouch(keys[i], out key, out isConvertable);
            //if (isConvertable)
            //    found.Add(key);
        }
        return found.ToArray();

    }
    private KeyboardTouch GetKeytouchOf(VirtualKeyCode keys)
    {
        KeyboardTouch key;
        bool isConvertable;
        KeyBindingTable.ConvertWindowVirtualKeyCodesToTouch(keys, out key, out isConvertable);
        
        return key;

    }

    public void RefreshState()
    {
        m_keyboardState.RefreshState();
    }

    public void AddWindowChangeListener(WindowSimListener.ChangeVirtualeKeyFound listener)
    {
        m_keyboardState.AddChangeListener(listener);
    }
    public void RemoveWindowChangeListener(WindowSimListener.ChangeVirtualeKeyFound listener)
    {
        m_keyboardState.RemoveChangeListener(listener);

    }
    public void AddWindowChangeListener(WindowSimListener.ChangeTouchFound listener)
    {
        m_keyboardState.AddChangeListener(listener);
    }
    public void RemoveWindowChangeListener(WindowSimListener.ChangeTouchFound listener)
    {
        m_keyboardState.RemoveChangeListener(listener);

    }
}

public class WindowSimListener {

    public List<VirtualKeyCode> m_observed = new List<VirtualKeyCode>();
    public List<VirtualKeyChange> m_keyState = new List<VirtualKeyChange>();


    public IEnumerable<VirtualKeyCode> GetPressing() { return m_keyState.Where(k => k.m_isPressed).Select(k => k.m_key); }
    public IEnumerable<VirtualKeyCode> GetReleased() { return m_keyState.Where(k => !k.m_isPressed).Select(k => k.m_key); }

    public void SetVirtualKey(params VirtualKeyCode [] keys) {
        m_observed.Clear();
        m_keyState.Clear();
        m_observed.AddRange(keys);
        for (int i = 0; i < keys.Length ; i++)
        {
            m_keyState.Add(new VirtualKeyChange(keys[i], false));
        }
    
    }

    List<VirtualKeyChange>m_changeFound = new List<VirtualKeyChange>();
    InputSimulator winKey = new InputSimulator();
    public void RefreshState() {
        VirtualKeyChange [] change;
        RefreshState(out change);
        if (change == null)
            return;
        if (m_changeKeyFoundCall == null  )
            return;
        for (int i = 0; i < change.Length; i++)
        {
            if (m_changeKeyFoundCall != null)
                m_changeKeyFoundCall.Invoke(change[i].GetPressState(), change[i].GetKey());
            if (m_changeTouchFoundCall != null)
                m_changeTouchFoundCall.Invoke(change[i].GetPressState(), GetKeytouchOf( change[i].GetKey() ));
        }

    }
    bool m_tmpConvertable;
    private KeyboardTouch GetKeytouchOf(VirtualKeyCode keys)
    {
        KeyboardTouch key;
        KeyBindingTable.ConvertWindowVirtualKeyCodesToTouch(keys, out key, out m_tmpConvertable);
        return key;

    }
    public void RefreshState(out VirtualKeyChange [] change) {
        m_changeFound.Clear();
        bool stateKey;
        for (int i = 0; i < m_keyState.Count; i++)
        {
            stateKey = winKey.InputDeviceState.IsHardwareKeyDown(m_keyState[i].GetKey());
            if (m_keyState[i].GetPressState() != stateKey) {
                m_keyState[i].SetAsPressed(stateKey);
                m_changeFound.Add(m_keyState[i]);
            }
        }
        if (m_changeFound.Count > 0)
            change = m_changeFound.ToArray();
        else change = null;
    }

    public ChangeVirtualeKeyFound m_changeKeyFoundCall;
    public ChangeTouchFound m_changeTouchFoundCall;
    public delegate void ChangeVirtualeKeyFound(bool pressState, VirtualKeyCode keyState);
    public delegate void ChangeTouchFound(bool pressState, KeyboardTouch keyState);
    public void AddChangeListener(ChangeVirtualeKeyFound listener)
    {
        m_changeKeyFoundCall += listener;
    }
    public void RemoveChangeListener(ChangeVirtualeKeyFound listener)
    {
        m_changeKeyFoundCall -= listener;
    }

    internal void AddChangeListener(ChangeTouchFound listener)
    {
        m_changeTouchFoundCall += listener;
    }

    internal void RemoveChangeListener(ChangeTouchFound listener)
    {
        m_changeTouchFoundCall -= listener;
    }
}

public class VirtualKeyChange
{
    public bool m_isPressed;
    public VirtualKeyCode m_key;

    public VirtualKeyChange(VirtualKeyCode virtualKeyCode, bool isPressed)
    {
        this.m_key = virtualKeyCode;
        this.m_isPressed = isPressed;
    }

    internal VirtualKeyCode GetKey()
    {
        return m_key;
    }

    internal bool GetPressState()
    {
        return m_isPressed;
    }

    internal void SetAsPressed(bool stateKey)
    {
        m_isPressed = stateKey;
    }
}

public class TouchKeyChange
{
    public bool m_isPressed;
    public KeyboardTouch m_key;

    public TouchKeyChange(KeyboardTouch virtualKeyCode, bool isPressed)
    {
        this.m_key = virtualKeyCode;
        this.m_isPressed = isPressed;
    }

    internal KeyboardTouch GetKey()
    {
        return m_key;
    }

    internal bool GetPressState()
    {
        return m_isPressed;
    }

    internal void SetAsPressed(bool stateKey)
    {
        m_isPressed = stateKey;
    }
}




[System.Serializable]
public class KeyFilterCollection_VirtualKeyCode {

    public List<KeyFilter_VirtualKeyCode> m_collection = new List<KeyFilter_VirtualKeyCode>();

    public KeyFilterCollection_VirtualKeyCode()
    {
        ResetWithAll();
    }

    public void ResetWithAll() {
        m_collection.Clear();
        foreach (VirtualKeyCode item in EnumUtility.GetEnumList<VirtualKeyCode>())
        {
            m_collection.Add(new KeyFilter_VirtualKeyCode(item, true));

        }
    }

    public VirtualKeyCode[] GetSelected()
    {
        return m_collection.Where(k => k.m_listenTo).Select(k=>k.vk_Key).ToArray();
    }
}



[System.Serializable]
public class KeyFilter_VirtualKeyCode {
    public VirtualKeyCode vk_Key;
    public bool m_listenTo;

    public KeyFilter_VirtualKeyCode(VirtualKeyCode vk_Key, bool listenTo)
    {
        this.vk_Key = vk_Key;
        m_listenTo = listenTo;
    }
}


[System.Serializable]
public class KeyFilterCollection_KeyboardTouch
{

    public List<KeyFilter_KeyboardTouch> m_collection = new List<KeyFilter_KeyboardTouch>();

    public KeyFilterCollection_KeyboardTouch()
    {
        ResetWithAll();
    }

    public void ResetWithAll()
    {
        m_collection.Clear();
        foreach (KeyboardTouch item in EnumUtility.GetEnumList<KeyboardTouch>())
        {
            m_collection.Add(new KeyFilter_KeyboardTouch(item, true));
        }
    }

    public KeyboardTouch[] GetSelected()
    {
        return m_collection.Where(k => k.m_listenTo).Select(k => k.vk_Key).ToArray();
    }
}
[System.Serializable]
public class KeyFilter_KeyboardTouch
{
    public KeyboardTouch vk_Key;
    public bool m_listenTo;

    public KeyFilter_KeyboardTouch(KeyboardTouch vk_Key, bool listenTo)
    {
        this.vk_Key = vk_Key;
        m_listenTo = listenTo;
    }
}


public class EnumUtility {
    public static List<G> GetEnumList<G>() where G : struct, IConvertible
    {
        return Enum.GetValues(typeof(G)).OfType<G>().ToList();
    }
}


///-------------------------------------------
///

[System.Serializable]
public class KeyConverterCollection_VirtualKeyCode
{

    public List<KeyConverter_VirtualKeyCode> m_collection = new List<KeyConverter_VirtualKeyCode>();

    public KeyConverterCollection_VirtualKeyCode()
    {
        ResetWithAll();
    }

    public void ResetWithAll()
    {
        m_collection.Clear();
        foreach (VirtualKeyCode item in EnumUtility.GetEnumList<VirtualKeyCode>())
        {
            m_collection.Add(new KeyConverter_VirtualKeyCode(item, item.ToString()));

        }
    }

    public string Get(VirtualKeyCode keyState)
    {
        foreach (KeyConverter_VirtualKeyCode item in m_collection)
        {
            if (item.vk_Key == keyState)
                return item.m_text;

        }
        return "";
    }
}



[System.Serializable]
public class KeyConverter_VirtualKeyCode
{
    public VirtualKeyCode vk_Key;
    public string m_text;

    public KeyConverter_VirtualKeyCode(VirtualKeyCode vk_Key, string text)
    {
        this.vk_Key = vk_Key;
        m_text = text;
    }
}


[System.Serializable]
public class KeyConverterCollection_KeyboardTouch
{

    public List<KeyConverter_KeyboardTouch> m_collection = new List<KeyConverter_KeyboardTouch>();

    public KeyConverterCollection_KeyboardTouch()
    {
        ResetWithAll();
    }

    public void ResetWithAll()
    {
        m_collection.Clear();
        foreach (KeyboardTouch item in EnumUtility.GetEnumList<KeyboardTouch>())
        {
            m_collection.Add(new KeyConverter_KeyboardTouch(item, item.ToString()));
        }
    }

    public string Get(KeyboardTouch keyState)
    {
        foreach (KeyConverter_KeyboardTouch item in m_collection)
        {
            if (item.vk_Key == keyState)
                return item.m_text;

        }
        return "";
    }
}
[System.Serializable]
public class KeyConverter_KeyboardTouch
{
    public KeyboardTouch vk_Key;
    public string m_text;

    public KeyConverter_KeyboardTouch(KeyboardTouch vk_Key, string text)
    {
        this.vk_Key = vk_Key;
        m_text = text;
    }
}
